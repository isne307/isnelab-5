#include <iostream>
#include <string>
#include <iostream>
#include <string>
#include <ctime>
#include "tree.h"
#include <ctime>
using namespace std;
int main()
{
	int num; //Add an interface 
	int numran;//Random numbers
	int del;//Deletion Operations
	int find; //find  out
	int t;//height of Trees
	int tn;//new height of Trees


	cout << "Add an interface which for Random numbers into a binary tree: ";
	cin >> num;
	cout << endl;
	cout << endl;

	Tree<int> mytree(num);
	Tree<int> balanceTree;

	cout << "Insert Random numbers into a binary tree :   " ;
	cout << endl;
	cout << endl;

	srand(time(0));
	for (int i = 0; i < num; i++) {
		numran = (rand() % 100) + 1;
		cout << numran << "  ";
		mytree.insert(numran);
	}

	t=mytree.height();
	cout << endl;
	cout << endl;
	cout << "The height of the Trees: " << t << endl;
	mytree.inorder();
	cout << endl;
	cout << endl;
	cout << endl;
	
	cout << "Balance the Trees:";
	cout << endl;
	cout << endl;
	balanceTree.balance(mytree.arr_tree,0,num-1);
	cout << endl;
	cout << endl;
	tn=balanceTree.height();
	cout << "The new height of the Trees:   " << tn << endl;

	cout << "What you want to find out in the Trees :   ";
	cin >> find;
	if (balanceTree.search(find)) 
	{
		cout << find << " Found in Trees." << endl;
	}
	else 
	{
		cout << find << " Not found in Trees." << endl;
	}

	cout << endl;

	cout << "Input number of operation to delete in Trees:  " ;
	cin >> del;
	cout << endl;
	
	for (int i= 0; i < del; i++) //random delete 
	{ 
				
			numran = (rand() % 100) + 1;
			cout << "Delete " << i+1 << "  ";
			cout << numran << "  ";
			if (balanceTree.search(numran)) 
			{
				cout << "in Trees" << endl;
				balanceTree.deletion(numran);
			}
	}
}